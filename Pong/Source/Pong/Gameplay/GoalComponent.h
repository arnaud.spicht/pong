// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "PongGameState.h"
#include "GoalComponent.generated.h"


UCLASS( ClassGroup=(Pong), meta=(BlueprintSpawnableComponent) )
class PONG_API UGoalComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGoalComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

protected:
	/** Player who scores */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pong")
	EPlayerEnum ForPlayer;

	/** Called when overlap other actor */
	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
};
