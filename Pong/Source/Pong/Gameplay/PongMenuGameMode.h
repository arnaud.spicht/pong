// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gameplay/PongGameMode.h"
#include "PongMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongMenuGameMode : public APongGameMode
{
	GENERATED_BODY()
	
public:
	APongMenuGameMode(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	/** called when the game is over */
	UFUNCTION()
	virtual void GameOver() override;
	
	
};
