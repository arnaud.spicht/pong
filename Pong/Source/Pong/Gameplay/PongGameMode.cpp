// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PongGameMode.h"
#include "PongGameState.h"
#include "Pawns/Paddle.h"
#include "Actors/Ball.h"
#include "Engine/TextRenderActor.h"

APongGameMode::APongGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultPawnClass = APaddle::StaticClass();
	GameStateClass = APongGameState::StaticClass();
	BallClass = ABall::StaticClass();
	ScoreToWin = 11;
}

void APongGameMode::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const World = GetWorld();
	if (World)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		const FVector Center(0, 0, 0);
		const FRotator Direction(0, 90, 0);
		MyBall = (ABall*)World->SpawnActor<ABall>(BallClass, Center, Direction, SpawnParams);
		if (MyBall)
		{
			// find launch direction
			FVector const LaunchDir(0, 1, 0);
			MyBall->InitVelocity(LaunchDir);
		}
		APongGameState* GameState = World->GetGameState<APongGameState>();
		if (GameState) {
			UE_LOG(PongLog, Log, TEXT("APongGameMode binding PlayerScored"));
			GameState->OnScoreChanged().AddUObject(this, &APongGameMode::PlayerScored);
		}
		
	}
	
}

void APongGameMode::PlayerScored(EPlayerEnum Player, uint8 Score)
{
	const FVector Center(0, 0, 0);
	UE_LOG(PongLog, Log, TEXT("APongGameMode Player Scored %d"), Score);
	if (!MyBall)
	{
		return;
	}


	if (Player == EPlayerEnum::PE_1)
	{

		FVector const LaunchDir(0, -1, 0);
		MyBall->InitVelocity(LaunchDir);		
	}
	else
	{
		FVector const LaunchDir(0, 1, 0);
		MyBall->InitVelocity(LaunchDir);
	}

	if (Score >= ScoreToWin)
	{
		GameOver();
	}
	else
	{
		MyBall->SetActorLocation(Center);
	}

}

void APongGameMode::GameOver()
{
}

void APongGameMode::RequestPause()
{
	OnPauseRequest();
}

void APongGameMode::OnPauseRequest_Implementation()
{
}


