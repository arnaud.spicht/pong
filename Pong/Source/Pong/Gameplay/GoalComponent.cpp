// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "GoalComponent.h"
#include "PongGameState.h"
#include "Actors/Ball.h"


// Sets default values for this component's properties
UGoalComponent::UGoalComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = false;
	ForPlayer = EPlayerEnum::PE_1;
}


// Called when the game starts
void UGoalComponent::BeginPlay()
{
	Super::BeginPlay();
	AActor* Owner = GetOwner();
	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(Owner->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (Mesh)
	{
		Mesh->OnComponentBeginOverlap.AddDynamic(this, &UGoalComponent::OnBeginOverlap);
	}
	
	// ...
	
}


// Called every frame
void UGoalComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UGoalComponent::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ABall* Ball = Cast<ABall>(OtherActor);
	if (Ball)
	{
		UWorld* TheWorld = GetWorld();
		APongGameState* GameState = TheWorld->GetGameState<APongGameState>();
		if (GameState)
		{
			GameState->PlayerScored(ForPlayer);
		}
	}
}

