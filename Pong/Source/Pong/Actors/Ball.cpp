// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "Ball.h"
#include "Pawns/Paddle.h"
#include "Gameplay/GoalComponent.h"

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create the static mesh component	
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BallMesh"));
	RootComponent = BallMesh;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MyMesh(TEXT("StaticMesh'/Game/Meshes/SM_Ball'"));
	if (MyMesh.Succeeded())
	{
		BallMesh->SetStaticMesh(MyMesh.Object);
	}

	BallMesh->SetSimulatePhysics(true);
	BallMesh->SetEnableGravity(false);
	BallMesh->SetLinearDamping(0.f);
	BallMesh->BodyInstance.bLockXRotation = true;
	BallMesh->BodyInstance.bLockYRotation = true;
	BallMesh->BodyInstance.bLockZRotation = true;
	BallMesh->BodyInstance.bLockZTranslation = true;
	
	//BallMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	//BallMesh->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
	BallMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Overlap);
	BallMesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Overlap);
	BallMesh->OnComponentBeginOverlap.AddDynamic(this, &ABall::OnBeginOverlap);
	InitialSpeed = 2000.f;

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called when the game starts or when spawned
void ABall::InitVelocity(const FVector& LaunchDirection)
{
	if (BallMesh)
	{
		// set the projectile's velocity to the desired direction
		BallMesh->SetPhysicsLinearVelocity(LaunchDirection * InitialSpeed);
	}

}

void ABall::OnBeginOverlap(AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	APaddle* Paddle = Cast<APaddle>(OtherActor);
	FVector Velocity = BallMesh->GetPhysicsLinearVelocity();
	
	UE_LOG(PongLog, Log, TEXT("OnBeginOverlap with %s"), *OtherActor->GetName());
	if (Paddle)
	{				
		FVector PaddleVelocity = Paddle->GetMesh()->GetPhysicsLinearVelocity();
		Velocity.Y *= -1;
		Velocity.X = PaddleVelocity.X + FMath::RandRange(-1000.f, 1000.f);
		BallMesh->SetPhysicsLinearVelocity(Velocity);
		OnHitObject(OtherActor);
	}
	else if (OtherComp->GetCollisionObjectType() == ECC_WorldStatic)
	{		
		FVector HitNormal = OtherActor->GetActorForwardVector();
		Velocity = Velocity.MirrorByVector(HitNormal);
		BallMesh->SetPhysicsLinearVelocity(Velocity);
		UGoalComponent* GoalComponent = Cast<UGoalComponent>(OtherActor->GetComponentByClass(UGoalComponent::StaticClass()));
		if (GoalComponent)
		{
			OnHitGoal(OtherActor);
		}
		else
		{
			OnHitObject(OtherActor);
		}
	}

}

void ABall::OnHitObject_Implementation(AActor * OtherActor)
{
	
}

void ABall::OnHitGoal_Implementation(AActor * OtherActor)
{
	
}