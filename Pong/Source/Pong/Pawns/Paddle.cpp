// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "Paddle.h"
#include "Gameplay/PongGameMode.h"


// Sets default values
APaddle::APaddle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ForceRate = 20000000.f;

	// Create the static mesh component	
	PaddleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMesh"));
	RootComponent = PaddleMesh;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MyMesh(TEXT("StaticMesh'/Game/Meshes/SM_Paddle'"));
	if (MyMesh.Succeeded())
	{
		PaddleMesh->SetStaticMesh(MyMesh.Object);
	}

	PaddleMesh->SetSimulatePhysics(true);
	PaddleMesh->SetEnableGravity(false);
	PaddleMesh->SetLinearDamping(10.f);
	PaddleMesh->BodyInstance.bLockXRotation = true;
	PaddleMesh->BodyInstance.bLockYRotation = true;
	PaddleMesh->BodyInstance.bLockZRotation = true;
	PaddleMesh->BodyInstance.bLockYTranslation = true;
	PaddleMesh->BodyInstance.bLockZTranslation = true;
}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APaddle::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAxis("MoveUp", this, &APaddle::MoveUp);
	InputComponent->BindAction("Pause", EInputEvent::IE_Pressed, this, &APaddle::Pause);

}

void APaddle::MoveUp(float Value)
{
	if (Value != 0.0f)
	{
		const FVector Force(ForceRate*Value, 0, 0);
		GetMesh()->AddForce(Force);		
	}
}

void APaddle::Pause()
{
	APongGameMode* GameMode = Cast<APongGameMode>(UGameplayStatics::GetGameMode(this));
	if (GameMode)
	{
		GameMode->RequestPause();
	}

}
