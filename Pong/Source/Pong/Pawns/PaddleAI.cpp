// Fill out your copyright notice in the Description page of Project Settings.

#include "Pong.h"
#include "PaddleAI.h"
#include "Actors/Ball.h"




// Sets default values
APaddleAI::APaddleAI()
	: Super()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ForceRate = 14000000.f;	

}

// Called when the game starts or when spawned
void APaddleAI::BeginPlay()
{
	Super::BeginPlay();
	FVector Origin;
	FVector BoxExtent;
	GetActorBounds(false, Origin, BoxExtent);
	UE_LOG(PongLog, Log, TEXT("APaddleAI Origin x:%f y:%f z:%f"), Origin.X, Origin.Y, Origin.Z);
	UE_LOG(PongLog, Log, TEXT("APaddleAI BoxExtent x:%f y:%f z:%f"), BoxExtent.X, BoxExtent.Y, BoxExtent.Z);
	MeshHeight = BoxExtent.GetMax();
	UE_LOG(PongLog, Log, TEXT("APaddleAI Mesh Height=%f"), MeshHeight);

}

// Called every frame
void APaddleAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float PaddleMeshLocationX = PaddleMesh->GetComponentLocation().X;
	if (!Ball)
	{
		return;
	}
	float BallLocationX = Ball->GetActorLocation().X;

	if (PaddleMeshLocationX > BallLocationX + MeshHeight/2)
	{
		
		MoveUp(-1.0f);
	}
	else if (PaddleMeshLocationX < BallLocationX - MeshHeight/2)
	{
		MoveUp(1.0f);
	}
	else
	{
		MoveUp(0.0f);
	}

}

void APaddleAI::SetBall(ABall *Ball)
{
	this->Ball = Ball;
}